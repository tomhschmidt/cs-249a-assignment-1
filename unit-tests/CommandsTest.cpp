#include "gtest/gtest.h"
#include "Commands.h"

TEST(Commands, addNewHelperCell) {

  Ptr<Tissue> tissue = Tissue::TissueNew("test");
  TissueReactor* reactor = TissueReactor::TissueReactorIs(tissue.ptr());
  Commands::AddCell(Cell::helperCell_, tissue.ptr(), 0, 0, 0);
  
  Cell::Coordinates coords;
  coords.x = 0;
  coords.y = 0;
  coords.z = 0;
  
  ASSERT_TRUE(tissue->cell(coords));
  ASSERT_TRUE(tissue->cell(coords)->cellType() == Cell::helperCell_);
  ASSERT_TRUE(tissue->cell(coords)->membrane(CellMembrane::north())->antibodyStrength().value() == 0);
}

TEST(Commands, addNewCytotoxicCell) {

  Ptr<Tissue> tissue = Tissue::TissueNew("test");
  TissueReactor* reactor = TissueReactor::TissueReactorIs(tissue.ptr());
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 0);
  
  Cell::Coordinates coords;
  coords.x = 0;
  coords.y = 0;
  coords.z = 0;
  
  ASSERT_TRUE(tissue->cell(coords));
  ASSERT_TRUE(tissue->cell(coords)->cellType() == Cell::cytotoxicCell_);
  ASSERT_TRUE(tissue->cell(coords)->membrane(CellMembrane::north())->antibodyStrength().value() == 100);
}

TEST(Commands, cloneCell) {
  Ptr<Tissue> tissue = Tissue::TissueNew("test");
  TissueReactor* reactor = TissueReactor::TissueReactorIs(tissue.ptr());
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 0);
  
  Cell::Coordinates coords;
  coords.x = 0;
  coords.y = 0;
  coords.z = 0;
  
  Ptr<Cell> cell = tissue->cell(coords);
  
  Commands::CloneCell(cell.ptr(), tissue.ptr(), CellMembrane::north());
  
  coords.y += 1;
  
  Ptr<Cell> clonedCell = tissue->cell(coords);
  ASSERT_TRUE(clonedCell);
}

TEST(Commands, removeInfectedCells) {
  Ptr<Tissue> tissue = Tissue::TissueNew("test");
  TissueReactor* reactor = TissueReactor::TissueReactorIs(tissue.ptr());
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 0);
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 1);
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 2);
  Commands::AddCell(Cell::cytotoxicCell_, tissue.ptr(), 0, 0, 3);
  
  Cell::Coordinates coords;
  coords.x = 0;
  coords.y = 0;
  coords.z = 0;
  
  Ptr<Cell> cell = tissue->cell(coords);
  cell->healthIs(Cell::infected());
  
  Commands::RemoveInfectedCells(tissue.ptr());
  Ptr<Cell> infectedCell = tissue->cell(coords);
  ASSERT_TRUE(!infectedCell);
}
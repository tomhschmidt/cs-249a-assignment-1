// #include <fstream>
// #include <stdlib.h>
// #include <limits.h>
// #include <queue>
// #include <map>
// #include "Tissue.h"
// #include "TissueReactor.h"
// 
// #include "Commands.h"
// #include "Utils.h"
// using namespace std;
// using namespace Fwk;
// 
// /*
//   The main takes in one input, the file name with the rules.
//   The rules are then executed and the appropriate statistics are printed
//   to the console.
// */
// 
// const string TISSUE_COMMAND_SPACE = "Tissue";
// const string CELL_COMMAND_SPACE = "Cell";
// 
// const string NEW_TISSUE_COMMAND = "tissueNew";
// 
// const string TISSUE_NEW_CT_CELL_COMMAND = "cytotoxicCellNew";
// const string TISSUE_NEW_H_CELL_COMMAND = "helperCellNew";
// const string TISSUE_START_INFECTION_COMMAND = "infectionStartLocationIs";
// const string TISSUE_REMOVE_INFECTED_CELLS_COMMAND = "infectedCellsDel";
// const string TISSUE_CLONE_COMMAND = "cloneCellsNew";
// 
// const string CELL_CLONE_COMMAND = "cloneNew";
// const string CELL_SET_ANTIBODY_STRENGTH_COMMAND = "antibodyStrengthIs";
// const string CELL_MEMBRANE_CONSTANT = "membrane";
// 
// 
// 
// // Add a default cell of the given cellType at the given coordinates on the given tissue
// void AddCell(Cell::CellType cellType, Tissue* tissue, int x, int y, int z) {
// 
//     Cell::Coordinates coords;
//     coords.x = x;
//     coords.y = y;
//     coords.z = z;
//     
//     AntibodyStrength antibodyStrength;
//     if(cellType == Cell::cytotoxicCell_) {
//         antibodyStrength.valueIs(100);
//     }
//     else {
//         antibodyStrength.valueIs(0);
//     }
// 
//     AntibodyStrength antibodyStrengthArray[6];
//     for(int side = 0; side <= 5;  side++) {
//         antibodyStrengthArray[side] = antibodyStrength;
//     }
//     
//     AddCell(cellType, tissue, coords, antibodyStrengthArray, Cell::healthy());
// }
// 
// void AddCell(Cell::CellType cellType, Tissue* tissue, Cell::Coordinates coords, AntibodyStrength* antibodyStrengthArray, Cell::HealthId cellHealth) {
//     Ptr<Cell> existingCell = tissue->cell(coords);
//     
//     // If there's already a cell at this location, throw an exception
//     if(existingCell) {
//         throw 20;
//         return;
//     }
//     else {
//         Ptr<TCell> newCell = TCell::TCellIs(coords, tissue, cellType);
//         tissue->cellIs(newCell);
//         
//         for(int side = 0; side <= 5;  side++) {
//             Ptr<CellMembrane> membrane = newCell->membrane(CellMembrane::SideInstance(side));
//             membrane->antibodyStrengthIs(antibodyStrengthArray[side]);
//         }
//         
//         newCell->healthIs(cellHealth);
//     }
// }
// 
// void CloneCell(const Cell* cell, Tissue* tissue, CellMembrane::Side direction) {
//     Cell::CellType cellType = cell->cellType();
//     Cell::Coordinates coords = cell->location();
//     
//     coords = MoveFromCoordinates(coords, direction);
//     
//     AntibodyStrength antibodyStrengthArray[6];
//     for(int side = 0; side <= 5; side++) {
//         antibodyStrengthArray[side] = cell->membrane(CellMembrane::SideInstance(side))->antibodyStrength();
//     }
//     
//     Cell::HealthId cellHealth = cell->health();
//     
//     AddCell(cellType, tissue, coords, antibodyStrengthArray, cellHealth);
// }
// 
// void StartInfection(Tissue* tissue, TissueReactor* reactor, int x, int y, int z, CellMembrane::Side direction, int infection) {
//     queue<Ptr<Cell> >* infectedCells = new queue<Ptr<Cell> >();
//     queue<Ptr<Cell> >* nextLevelCells = new queue<Ptr<Cell> >();
//     
//     int directionOrder[] = {0, 2, 1, 3, 4, 5};
//     
//     int maxDepth = 0, strengthDifference = 0, numInfectionAttempts = 0;
//     
//     AntibodyStrength infectionStrength;
//     infectionStrength.valueIs(infection);
//     
//     Cell::Coordinates coords;
//     coords.x = x;
//     coords.y = y;
//     coords.z = z;
//     
//     Ptr<Cell> startingCell = tissue->cell(coords);
//     
//     if(startingCell && startingCell->health() == 1) {
//         numInfectionAttempts++;
//         strengthDifference += (infectionStrength.value() - startingCell->membrane(direction)->antibodyStrength().value());
//         
//         if(infectionStrength > startingCell->membrane(direction)->antibodyStrength()) {
//             startingCell->healthIs(Cell::infected());
//             infectedCells->push(startingCell);
//             
//             while(true) {
//                 while(!infectedCells->empty()) {
//                     Ptr<Cell> currentCell = infectedCells->front();
//                     infectedCells->pop();
//                     Cell::Coordinates currentCoords = currentCell->location();
//                 
//                     for(int directionIndex = 0; directionIndex < 6; directionIndex++) {
//                         CellMembrane::Side direction = CellMembrane::SideInstance(directionOrder[directionIndex]);
//                         Cell::Coordinates newCoords = MoveFromCoordinates(currentCoords, direction);
//                         Ptr<Cell> neighborCell = tissue->cell(newCoords);
//                         if(neighborCell && neighborCell->health() == 1) {
//                             numInfectionAttempts++;
//                             AntibodyStrength neighborStrength = neighborCell->membrane(GetOppositeDirection(direction))->antibodyStrength();
//                             strengthDifference += (infectionStrength.value() - neighborStrength.value());
//                             if(neighborStrength < infectionStrength) {
//                                 neighborCell->healthIs(Cell::infected());
//                                 nextLevelCells->push(neighborCell);
//                             }
//                         }
//                     }
//                 }
//                 
//                 maxDepth++;
//                 if(!nextLevelCells->empty()) {
//                     queue<Ptr<Cell> >* dummy = infectedCells;
//                     infectedCells = nextLevelCells;
//                     nextLevelCells = dummy;
//                 }
//                 else{
//                     break;
//                 }
//             }
//         }
//     }
//     
//     int numInfectedCells = 0;
//     int largestX = INT_MIN, smallestX = INT_MAX, largestY = INT_MIN, smallestY = INT_MAX, largestZ = INT_MIN, smallestZ = INT_MAX;
//     for (Tissue::CellIteratorConst i = tissue->cellIterConst(); i; ++i) {
//       if(i.ptr()->health() == Cell::infected_) {
//         numInfectedCells++;
//         
//         Cell::Coordinates coords = i.ptr()->location();
//         if(coords.x > largestX) {
//             largestX = coords.x;
//         }
//         if(coords.x < smallestX) {
//             smallestX = coords.x;
//         }
//         
//         if(coords.y > largestY) {
//             largestY = coords.y;
//         }
//         if(coords.y < smallestY) {
//             smallestY = coords.y;
//         }
//         
//         if(coords.z > largestZ) {
//             largestZ = coords.z;
//         }
//         if(coords.z < smallestZ) {
//             smallestZ = coords.z;
//         }
//       }
//     }
//     
//     int infectedArea = (numInfectedCells == 0 ? 0 : (largestX - smallestX + 1) * (largestY - smallestY + 1) * (largestZ - smallestZ + 1));
//     
//     printf("%d %d %d %d %d %d %d\n", numInfectedCells, numInfectionAttempts, strengthDifference, reactor->numCellsOfType(Cell::cytotoxicCell_), reactor->numCellsOfType(Cell::helperCell_), infectedArea, maxDepth);
//     
//     delete nextLevelCells;
//     delete infectedCells;
// }
// 
// void RemoveInfectedCells(Tissue* tissue) {
//     vector<const Cell*> cellsToRemove;
//     for(Tissue::CellIteratorConst cellIterator = tissue->cellIter(); cellIterator; ++cellIterator) {
//         if(cellIterator.ptr()->health() == Cell::infected_) {
//             cellsToRemove.push_back(cellIterator.ptr());
//         }
//     }
//     
//     int numInfectedCells = cellsToRemove.size();
//     for(int i = 0; i < numInfectedCells; ++i) {
//         tissue->cellDel(cellsToRemove[i]->name());
//     }
// }

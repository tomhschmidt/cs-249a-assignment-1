#ifndef TISSUE_REACTOR_H
#define TISSUE_REACTOR_H

#include "Tissue.h"
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <map>

using namespace std;
using namespace Fwk;

class TissueReactor : public Tissue::Notifiee {
public:
   void onCellNew(Cell::Ptr cell) {
        for(int side = 0; side <= 5;  side++) {
            string membraneName = cell->name();
            membraneName.push_back(CellMembrane::SideInstance(side));
            cell->membraneNew(membraneName, CellMembrane::SideInstance(side));
        }
        
        this->cellCounts[cell->cellType()]++;
   }
   
   void onCellDel(Cell::Ptr cell) {
        this->cellCounts[cell->cellType()]--;
        
        for(int side = 0; side <= 5;  side++) {
            cell->membraneDel(CellMembrane::SideInstance(side));
        }
   }
   
   U32 numCellsOfType(Cell::CellType cellType) {
        return cellCounts[cellType];
   }

   static TissueReactor * TissueReactorIs(Tissue *t) {
      TissueReactor *m = new TissueReactor(t);
      return m;
   }
   
protected:
    map<Cell::CellType, U32> cellCounts;
   TissueReactor(Tissue *t) : Tissue::Notifiee() {
      notifierIs(t);
   }
};

#endif
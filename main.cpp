#include <fstream>
#include <stdlib.h>
#include <limits.h>
#include <queue>
#include <map>
#include "Tissue.h"
#include "TissueReactor.h"
#include "Utils.h"
#include "Commands.h"
using namespace std;
using namespace Fwk;

/*
  The main takes in one input, the file name with the rules.
  The rules are then executed and the appropriate statistics are printed
  to the console.
*/

const string TISSUE_COMMAND_SPACE = "Tissue";
const string CELL_COMMAND_SPACE = "Cell";

const string NEW_TISSUE_COMMAND = "tissueNew";

const string TISSUE_NEW_CT_CELL_COMMAND = "cytotoxicCellNew";
const string TISSUE_NEW_H_CELL_COMMAND = "helperCellNew";
const string TISSUE_START_INFECTION_COMMAND = "infectionStartLocationIs";
const string TISSUE_REMOVE_INFECTED_CELLS_COMMAND = "infectedCellsDel";
const string TISSUE_CLONE_COMMAND = "cloneCellsNew";

const string CELL_CLONE_COMMAND = "cloneNew";
const string CELL_SET_ANTIBODY_STRENGTH_COMMAND = "antibodyStrengthIs";
const string CELL_MEMBRANE_CONSTANT = "membrane";

void ProcessInstructions(vector<string>* tokens);
void Setup();

map<string, Ptr<Tissue> >* tissues;
map<string, TissueReactor* >* tissueReactors;

int main(int argc, const char* argv[]) {
  ifstream infile(argv[1]);
  if(infile.fail()){
    //File error. Halt program.
    cout << "error reading file" << endl;
    return 1;
  }
  
  Setup();

  //read data in, parse it, excute commands.
  string textLine;
  const char* delimiters = TOKEN_SEPARATOR.c_str();
  while(!infile.eof()){
    getline(infile, textLine);
    vector<string>* tokens = TokenizeInputLine(textLine, delimiters);
    if(tokens) {
        ProcessInstructions(tokens);
    }
  }
  return 0;
}

// Initialize instances of global / status variables
void Setup() {
    tissues = new map<string, Ptr<Tissue> >();
    tissueReactors = new map<string, TissueReactor* >();
}


// Take tokens from input string and try to parse into instructions
// This could probably be decomposed better
void ProcessInstructions(vector<string>* tokens) {
    unsigned int curIndex = 0;
    int numTokens = tokens->size();
    
    if(!tokens->at(curIndex).compare(TISSUE_COMMAND_SPACE)) {
        // Better token num checking here
        assert(++curIndex < numTokens);
        
        if(!tokens->at(curIndex).compare(NEW_TISSUE_COMMAND)) {
        
            // Add new Tissue
            assert(numTokens == 3);
            
            string tissueName = tokens->at(++curIndex);
            Ptr<Tissue> newTissue = Tissue::TissueNew(tissueName);
            (*tissues)[tissueName] = newTissue;
            TissueReactor* reactor = TissueReactor::TissueReactorIs(newTissue.ptr());
            (*tissueReactors)[tissueName] = reactor;
        }
        else {
        
            // Assuming this token is a Tissue name
            string tissueName = tokens->at(curIndex);
            
            if(tissues->find(tissueName) == tissues->end()) {
                return;
            }
            
            Ptr<Tissue> curTissue = tissues->at(tissueName);
            if(curTissue) {
                assert(++curIndex < numTokens);
                
                if(!tokens->at(curIndex).compare(TISSUE_NEW_CT_CELL_COMMAND) || !tokens->at(curIndex).compare(TISSUE_NEW_H_CELL_COMMAND)) {
                    assert(numTokens == 6);
                    
                    Cell::CellType cellType;
                    if(!tokens->at(curIndex).compare(TISSUE_NEW_CT_CELL_COMMAND)) {
                        cellType = Cell::cytotoxicCell_;
                    }
                    else {
                        cellType = Cell::helperCell_;
                    }
                    

                    int x = atoi(tokens->at(++curIndex).c_str());
                    int y = atoi(tokens->at(++curIndex).c_str());
                    int z = atoi(tokens->at(++curIndex).c_str());
                    
                    try {
                        Commands::AddCell(cellType, curTissue.ptr(), x, y, z);
                    }
                    catch (int e) {
                    }
                }
                else if(!tokens->at(curIndex).compare(TISSUE_START_INFECTION_COMMAND)) {
                    assert(numTokens == 8);
                    
                    int x = atoi(tokens->at(++curIndex).c_str());
                    int y = atoi(tokens->at(++curIndex).c_str());
                    int z = atoi(tokens->at(++curIndex).c_str());
                    
                    CellMembrane::Side direction = CellMembrane::SideInstance(tokens->at(++curIndex).c_str());
                    
                    int infectionStrength = atoi(tokens->at(++curIndex).c_str());
                    
                    TissueReactor* tissueReactor = tissueReactors->at(curTissue->name());
                    Commands::StartInfection(curTissue.ptr(), tissueReactor, x, y, z, direction, infectionStrength);
                }
                else if(!tokens->at(curIndex).compare(TISSUE_REMOVE_INFECTED_CELLS_COMMAND)) {
                    Commands::RemoveInfectedCells(curTissue.ptr());
                }
                else if(!tokens->at(curIndex).compare(TISSUE_CLONE_COMMAND)) {
                    assert(numTokens == 4);
                    
                    CellMembrane::Side direction = CellMembrane::SideInstance(tokens->at(++curIndex).c_str());
                    
                    vector<const Cell*> cellsToClone;
                    Tissue::CellIteratorConst cellIterator = curTissue->cellIter();
                    while(cellIterator.ptr()) {
                        cellsToClone.push_back(cellIterator.ptr());
                        ++cellIterator;
                    }
                    
                    int numCellsToClone = cellsToClone.size();
                    for(int i = 0; i < numCellsToClone; ++i) {
                        try {
                            Commands::CloneCell(cellsToClone[i], curTissue.ptr(), direction);
                        } catch(int e) {
                        }
                    }
                    
                }
                else {
                    // Invalid command
                }
            }
            else {
                // Tissue name does not exist
            }
        }
    }
    else if(!tokens->at(curIndex).compare(CELL_COMMAND_SPACE)) {
        assert(++curIndex < numTokens);
        
        string tissueName = tokens->at(curIndex);
        
        if(tissues->find(tissueName) == tissues->end()) {
                return;
        }
        
        Ptr<Tissue> curTissue = tissues->at(tissueName);
        
        if(curTissue) {
            assert(numTokens >= 7);
        
            int x = atoi(tokens->at(++curIndex).c_str());
            int y = atoi(tokens->at(++curIndex).c_str());
            int z = atoi(tokens->at(++curIndex).c_str());
            
            Cell::Coordinates coords;
            coords.x = x;
            coords.y = y;
            coords.z = z;
            
            curIndex++;
            
            if(!tokens->at(curIndex).compare(CELL_CLONE_COMMAND)) {
                assert(numTokens == 7);
                
                CellMembrane::Side direction = CellMembrane::SideInstance(tokens->at(++curIndex).c_str());
                Ptr<Cell> cellToClone = curTissue->cell(coords);
                if(cellToClone) {
                    try {
                        Commands::CloneCell(cellToClone.ptr(), curTissue.ptr(), direction);
                    } catch(int e) {
                    }
                }
            }
            else if(!tokens->at(curIndex).compare(CELL_MEMBRANE_CONSTANT)) {
                assert(numTokens == 9);
                
                Ptr<Cell> cellToModify = curTissue->cell(coords);
                if(cellToModify) {
                    CellMembrane::Side direction = CellMembrane::SideInstance(tokens->at(++curIndex).c_str());
                    assert(!tokens->at(++curIndex).compare(CELL_SET_ANTIBODY_STRENGTH_COMMAND));
                
                    AntibodyStrength antibodyStrength;
                    int abStrengthValue = atoi(tokens->at(++curIndex).c_str());
                    antibodyStrength.valueIs(abStrengthValue);
                    
                    Ptr<CellMembrane> curMembrane = cellToModify->membrane(direction);
                    curMembrane->antibodyStrengthIs(antibodyStrength);
                }
                
            }
            else {
                // Invalid command
            }
        }
        else {
            // Tissue name does not exist
        }
    }
    
    delete tokens;
}

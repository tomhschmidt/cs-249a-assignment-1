#ifndef UTILS_H
#define UTILS_H

#include "Tissue.h"

const char COMMENT_CHARACTER = '#';
const string TOKEN_SEPARATOR = " ";

// Take line from input file and conver to vector of space-separated strings
vector<string>* TokenizeInputLine(string inputLine, const char* delimiters) {
    
    if(inputLine.size() == 0 || inputLine[0] == COMMENT_CHARACTER) {
        return NULL;
    }
    
    vector<string>* tokens = new vector<string>();
    char* cStringInput = strdup(inputLine.c_str());
    const char* curToken = strtok(cStringInput, delimiters);
    while(curToken) {
        tokens->push_back(string(curToken));
        curToken = strtok(NULL, delimiters);
    }
    
    free(cStringInput);
    return tokens;
}

Cell::Coordinates MoveFromCoordinates(Cell::Coordinates coords, CellMembrane::Side direction) {

    switch(direction) {
        case CellMembrane::north_:
            coords.y++;
            break;
        case CellMembrane::south_:
            coords.y--;
            break;
        case CellMembrane::east_:
            coords.x++;
            break;
        case CellMembrane::west_:
            coords.x--;
            break;
        case CellMembrane::up_:
            coords.z++;
            break;
        case CellMembrane::down_:
            coords.z--;
            break;
        default:
            break;
    }
    
    return coords;
}

CellMembrane::Side GetOppositeDirection(CellMembrane::Side direction) {
   switch(direction) {
        case CellMembrane::north_:
            return CellMembrane::south_;
            break;
        case CellMembrane::south_:
            return CellMembrane::north_;
            break;
        case CellMembrane::east_:
            return CellMembrane::west_;
            break;
        case CellMembrane::west_:
            return CellMembrane::east_;
            break;
        case CellMembrane::up_:
            return CellMembrane::down_;
            break;
        case CellMembrane::down_:
            return CellMembrane::up_;
            break;
        default:
            break;
   }
}

#endif